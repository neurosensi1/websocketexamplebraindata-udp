﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebSocketSharp;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net;

namespace WebSocketExampleBrainData
{
    class Program
    {
        //private const string URL = "192.168.1.19:9000";
        private const string URL = "localhost:9000";
        //private const string URL = "52.31.154.40:9000";
        private const string webSocketEndpoint = "/ws-storeBrainData";
        private static int userID;

        public static void Main(string[] args)
        {
            ServicePointManager.ServerCertificateValidationCallback =
    delegate (object s, X509Certificate certificate,
             X509Chain chain, SslPolicyErrors sslPolicyErrors)
    { return true; };
            int savedSessionID = 0;
            DateTime dateTime = DateTime.Now;
            HttpClient client = new HttpClient();
            Console.WriteLine("Welcome to the console application for NeuroConcise!");
            Console.WriteLine("Please login to the system using your username and password.");
            Console.WriteLine("Username:");
            String username = Console.ReadLine();
            Console.WriteLine("Password:");
            String password = ReadPassword();

            client.BaseAddress = new Uri("https://" + URL + "/checkDetailsClient");

            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("possibleUsername", username),
                new KeyValuePair<string, string>("possiblePassword", password)
            });
            var result = client.PostAsync("https://" + URL + "/checkDetailsClient", content).Result;
            string resultContent = result.Content.ReadAsStringAsync().Result;

            if (Convert.ToInt32(resultContent) != -1)
            {
                userID = Convert.ToInt32(resultContent);
                //Continue logic
                Console.WriteLine("You have successfully logged in! Your UserID is: " + userID);
                Console.WriteLine("To start a session, please press the enter key.");
                Console.ReadKey();
                String urlParameters = "?userID="
                + userID + "&startTime=" +
                dateTime.ToString("yyyy:MM:dd:hh:mm:ss:fff").Replace(" ", String.Empty).Replace("/", "-");
                client = new HttpClient();
                client.BaseAddress = new Uri("https://" + URL + "/startSession");

                HttpResponseMessage startResponse = client.GetAsync(urlParameters).Result;
                if (startResponse.IsSuccessStatusCode)
                {
                    var sessionID = startResponse.Content.ReadAsAsync<int>().Result;
                    savedSessionID = sessionID;
                    Console.WriteLine("Session ID is: " + sessionID);
                    Console.WriteLine("Press the return key to start streaming");
                    Console.ReadKey();


                    using (var ws = new WebSocket("wss://" + URL + webSocketEndpoint))
                    {
                        ws.Connect();
                        ws.OnMessage += (sender, e) =>
                        Console.WriteLine("Server says: " + e.Data);
                        Console.WriteLine("DO you wish to read from a file or UDP? Please enter 1 for file, 2 for UDP.");
                        int choice = Convert.ToInt32(Console.ReadLine());
                        if (choice == 1)
                        {
                            List<BrainData> brainDataList = new List<BrainData>();
                            BrainData brainData;

                            var reader = new StreamReader(File.OpenRead(@"B:\damien_coney\BrainDataWebService\DataExample.csv"));
                            int count = 0;
                            var watch = System.Diagnostics.Stopwatch.StartNew();
                            Console.WriteLine("Starting reading file");
                            while (!reader.EndOfStream)
                            {
                                var line = reader.ReadLine();
                                if (!line.Contains("EEG"))
                                {
                                    var values = line.Split(',');
                                    dateTime = dateTime.AddMilliseconds(8);
                                    brainData = new BrainData(userID,
                                                              dateTime.ToString("yyyy:MM:dd:hh:mm:ss:fff").Replace(" ", String.Empty).Replace("/", "-"),
                                                              Convert.ToDouble(values[0]),
                                                              Convert.ToDouble(values[1]),
                                                              Convert.ToDouble(values[2]),
                                                              Convert.ToDouble(1),
                                                              Convert.ToDouble(1),
                                                              Convert.ToDouble(1),
                                                              Convert.ToDouble(1),
                                                              Convert.ToDouble(1),
                                                              Convert.ToDouble(1),
                                                              Convert.ToDouble(1),
                                                              Convert.ToDouble(1),
                                                              Convert.ToDouble(1),  
                                                              Convert.ToDouble(1),
                                                              Convert.ToDouble(1),
                                                              Convert.ToDouble(1),
                                                              Convert.ToDouble(1),
                                                              Convert.ToInt32(values[3]),
                                                              Convert.ToInt32(values[4]),
                                                              Convert.ToDouble(values[5]),
                                                              Convert.ToInt32(values[6]),
                                                              sessionID);
                                    String stringToSend = "{ 'userId':" + brainData.getUserID() + ",'collectionTime':'" + brainData.getCollectionTime()
                + "','channelOne':" + brainData.getChannelOne() + ",'channelTwo':" + brainData.getChannelTwo() +
                ",'channelThree':" + brainData.getChannelThree()
                + ",'channelFour':" + brainData.getChannelFour()
                + ",'channelFive':" + brainData.getChannelFive()
                + ",'channelSix':" + brainData.getChannelSix()
                + ",'channelSeven':" + brainData.getChannelSeven()
                + ",'channelEight':" + brainData.getChannelEight()
                + ",'channelNine':" + brainData.getChannelNine()
                + ",'channelTen':" + brainData.getChannelTen()
                + ",'channelEleven':" + brainData.getChannelEleven()
                + ",'channelTwelve':" + brainData.getChannelTwelve()
                + ",'channelThirteen':" + brainData.getChannelThirteen()
                + ",'channelFourteen':" + brainData.getChannelFourteen()
                + ",'channelFifteen':" + brainData.getChannelFifteen()
                + ",'channelSixteen':" + brainData.getChannelSixteen()
                + ",'leftOrRightTrigger':" + brainData.getLeftOrRightTrigger() + ",'binaryClass':" + brainData.getBinaryClass()
                + ",'timeVaryingSignedDistance':" + brainData.getTimeVaryingSignedDistance() + ",'cybathlon':" + brainData.getCybathlon() + ",'sessionId':" + brainData.getSessionID() + "}";
                                    ws.Send(stringToSend);
                                    //brainDataList.Add(brainData);
                                }
                                count++;
                            }
                            Console.WriteLine("Finished reading file. Press enter to continue");
                            Console.ReadKey();
                        } else
                        {
                            UDPServer udpServer = new UDPServer(ws, sessionID, userID);
                            udpServer.Start();

                            Console.WriteLine("To finish streaming, press the enter key at any time.");
                            Console.ReadKey();
                            udpServer.OnApplicationQuit();
                        }

                        ws.Close();
                        client = new HttpClient();
                        
                        String urlParametersTwo = "?sessionID="
                            + savedSessionID + "&endTime=" +
                            dateTime.ToString("yyyy:MM:dd:hh:mm:ss:fff").Replace(" ", String.Empty).Replace("/", "-");

                        client.BaseAddress = new Uri("https://" + URL + "/endSession");


                        HttpResponseMessage endResponse = client.GetAsync(urlParametersTwo).Result;
                        if (endResponse.IsSuccessStatusCode)
                        {
                            Console.WriteLine("Session has ended. Press enter to quit ");
                            Console.ReadKey();
                        }
                    }
                }
                else
                {
                    Console.WriteLine("{0} ({1})", (int)startResponse.StatusCode, startResponse.ReasonPhrase);
                    Console.ReadLine();
                }
            }
            else
            {
                //Restart application
            }
        }

        public static string ReadPassword()
        {
            string password = "";
            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter)
            {
                if (info.Key != ConsoleKey.Backspace)
                {
                    Console.Write("*");
                    password += info.KeyChar;
                }
                else if (info.Key == ConsoleKey.Backspace)
                {
                    if (!string.IsNullOrEmpty(password))
                    {
                        // remove one character from the list of password characters
                        password = password.Substring(0, password.Length - 1);
                        // get the location of the cursor
                        int pos = Console.CursorLeft;
                        // move the cursor to the left by one character
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                        // replace it with space
                        Console.Write(" ");
                        // move the cursor to the left by one character again
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                    }
                }
                info = Console.ReadKey(true);
            }
            // add a new line because user pressed enter at the end of their password
            Console.WriteLine();
            return password;
        }
    }
}
