﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSocketExampleBrainData
{
    class BrainData
    {
        private int userID;
        private String collectionTime;
        private double channelOne;
        private double channelTwo;
        private double channelThree;
        private double channelFour;
        private double channelFive;
        private double channelSix;
        private double channelSeven;
        private double channelEight;
        private double channelNine;
        private double channelTen;
        private double channelEleven;
        private double channelTwelve;
        private double channelThirteen;
        private double channelFourteen;
        private double channelFifteen;
        private double channelSixteen;
        private int leftOrRightTrigger;
        private int binaryClass;
        private double timeVaryingSignedDistance;
        private int cybathlon;
        private int sessionId;

        public BrainData()
        {

        }

        public BrainData(int userID, String collectionTime,
            double channelOne, double channelTwo, double channelThree, 
            double channelFour, double channelFive, double channelSix, double channelSeven, double channelEight,
            double channelNine, double channelTen, double channelEleven, double channelTwelve,
            double channelThirteen, double channelFourteen, double channelFifteen, double channelSixteen,
            int leftOrRightTrigger, int binaryClass, double timeVaryingSignedDistance, int cybathlon, int sessionId)
        {
            this.userID = userID;
            this.collectionTime = collectionTime;
            this.channelOne = channelOne;
            this.channelTwo = channelTwo;
            this.channelThree = channelThree;
            this.channelFour = channelFour;
            this.channelFive = channelFive;
            this.channelSix = channelSix;
            this.channelSeven = channelSeven;
            this.channelEight = channelEight;
            this.channelNine = channelNine;
            this.channelTen = channelTen;
            this.channelEleven = channelEleven;
            this.channelTwelve = channelTwelve;
            this.channelThirteen = channelThirteen;
            this.channelFourteen = channelFourteen;
            this.channelFifteen = channelFifteen;
            this.channelSixteen = channelSixteen;
            this.leftOrRightTrigger = leftOrRightTrigger;
            this.binaryClass = binaryClass;
            this.timeVaryingSignedDistance = timeVaryingSignedDistance;
            this.cybathlon = cybathlon;
            this.sessionId = sessionId;
        }

        public int getUserID()
        {
            return this.userID;
        }

        public String getCollectionTime()
        {
            return this.collectionTime;
        }

        public double getChannelOne()
        {
            return this.channelOne;
        }

        public double getChannelTwo()
        {
            return this.channelTwo;
        }

        public double getChannelThree()
        {
            return this.channelThree;
        }

        public double getChannelFour()
        {
            return this.channelFour;
        }

        public double getChannelFive()
        {
            return this.channelFive;
        }

        public double getChannelSix()
        {
            return this.channelSix;
        }

        public double getChannelSeven()
        {
            return this.channelSeven;
        }

        public double getChannelEight()
        {
            return this.channelEight;
        }

        public double getChannelNine()
        {
            return this.channelNine;
        }

        public double getChannelTen()
        {
            return this.channelTen;
        }

        public double getChannelEleven()
        {
            return this.channelEleven;
        }

        public double getChannelTwelve()
        {
            return this.channelTwelve;
        }

        public double getChannelThirteen()
        {
            return this.channelThirteen;
        }

        public double getChannelFourteen()
        {
            return this.channelFourteen;
        }
        
        public double getChannelFifteen()
        {
            return this.channelFifteen;
        }

        public double getChannelSixteen()
        {
            return this.channelSixteen;
        }

        public int getLeftOrRightTrigger()
        {
            return this.leftOrRightTrigger;
        }

        public int getBinaryClass()
        {
            return this.binaryClass;
        }

        public double getTimeVaryingSignedDistance()
        {
            return this.timeVaryingSignedDistance;
        }

        public int getCybathlon()
        {
            return this.cybathlon;
        }

        public int getSessionID()
        {
            return this.sessionId;
        }
    }
}
