﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace WebSocketExampleBrainData
{
    class UDPServer
    {
        private static IPEndPoint receivingEndPoint = new IPEndPoint(IPAddress.Any, 3002);
        private static UdpClient client;
        private Thread t;
        private WebSocketSharp.WebSocket ws;
        private int sessionID;
        private int userID;

        public UDPServer(WebSocketSharp.WebSocket ws, int sessionID, int userID)
        {
            this.ws = ws;
            this.sessionID = sessionID;
            this.userID = userID;
        }

        public void Start()
        {
            try
            {
                client = new UdpClient(3002);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            t = new Thread(new ThreadStart(ReceiveSignal));
            t.Start();
        }

        public void ReceiveSignal()
        {
            while (true)
            {
                DateTime dateTime = DateTime.Now;
                Byte[] recvMsg = client.Receive(ref receivingEndPoint);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(recvMsg);
                }
                double channelOne = BitConverter.ToDouble(recvMsg, 0);
                double channelTwo = BitConverter.ToDouble(recvMsg, 8);
                double channelThree = BitConverter.ToDouble(recvMsg, 17);
                double channelFour = BitConverter.ToDouble(recvMsg, 26);
                double channelFive = BitConverter.ToDouble(recvMsg, 35);
                double channelSix = BitConverter.ToDouble(recvMsg, 44);
                double channelSeven = BitConverter.ToDouble(recvMsg, 53);
                double channelEight = BitConverter.ToDouble(recvMsg, 62);
                double channelNine = BitConverter.ToDouble(recvMsg, 71);
                double channelTen = BitConverter.ToDouble(recvMsg, 80);
                double channelEleven = BitConverter.ToDouble(recvMsg, 89);
                double channelTwelve = BitConverter.ToDouble(recvMsg, 98);
                double channelThirteen = BitConverter.ToDouble(recvMsg, 107);
                double channelFourteen = BitConverter.ToDouble(recvMsg, 116);
                double channelFifteen = BitConverter.ToDouble(recvMsg, 125);
                double channelSixteen = BitConverter.ToDouble(recvMsg, 134);
                //double leftOrRightTrigger = BitConverter.ToDouble(recvMsg, 26);
                //double binaryClass = BitConverter.ToDouble(recvMsg, 35);
                //double tsvd = BitConverter.ToDouble(recvMsg, 44);
                //double cybathlon = BitConverter.ToDouble(recvMsg, 53);
                Console.WriteLine("Channel One: " + channelOne);
                //Console.WriteLine("Channel Two: " + channelTwo);
                //Console.WriteLine("Channel Three: " + channelThree);
                //Console.WriteLine("Channel Four: " + channelFour);
                //Console.WriteLine("Channel Five: " + channelFive);
                //Console.WriteLine("Channel Six: " + channelSix);
                //Console.WriteLine("Channel Seven: " + channelSeven);
                //Console.WriteLine("Channel Eight: " + channelEight);
                //Console.WriteLine("Channel Nine: " + channelNine);
                //Console.WriteLine("Channel Ten: " + channelTen);
                //Console.WriteLine("Channel Eleven: " + channelEleven);
                //Console.WriteLine("Channel Twelve: " + channelTwelve);
                //Console.WriteLine("Channel Thirteen: " + channelThirteen);
                //Console.WriteLine("Channel Fourteen: " + channelFourteen);
                //Console.WriteLine("Channel Fifteen: " + channelFifteen);
                //Console.WriteLine("Channel Sixteen: " + channelSixteen);
                dateTime = dateTime.AddMilliseconds(4);
                BrainData brainData;
                /*brainData = new BrainData(userID,
                                          dateTime.ToString("yyyy:MM:dd:hh:mm:ss:fff").Replace(" ", String.Empty).Replace("/", "-"),
                                          channelOne, //The udp data converted to double
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToDouble(1),
                                          Convert.ToInt32(1),
                                          Convert.ToInt32(1),
                                          Convert.ToDouble(1),
                                          Convert.ToInt32(1),
                                          sessionID);*/
                
                brainData = new BrainData(userID,
                                          dateTime.ToString("yyyy:MM:dd:hh:mm:ss:fff").Replace(" ", String.Empty).Replace("/", "-"),
                                          channelOne, //The udp data of all values converted to double
                                          channelTwo,
                                          channelThree,
                                          channelFour,
                                          channelFive,
                                          channelSix,
                                          channelSeven,
                                          channelEight,
                                          channelNine,
                                          channelTen,
                                          channelEleven,
                                          channelTwelve,
                                          channelThirteen,
                                          channelFourteen,
                                          channelFifteen,
                                          channelSixteen,
                                          Convert.ToInt32(1),
                                          Convert.ToInt32(1),
                                          Convert.ToDouble(1),
                                          Convert.ToInt32(1),
                                          sessionID);
                                          
                String stringToSend = "{ 'userId':" + brainData.getUserID() + ",'collectionTime':'" + brainData.getCollectionTime()
                + "','channelOne':" + brainData.getChannelOne() + ",'channelTwo':" + brainData.getChannelTwo() +
                ",'channelThree':" + brainData.getChannelThree()
                + ",'channelFour':" + brainData.getChannelFour()
                + ",'channelFive':" + brainData.getChannelFive()
                + ",'channelSix':" + brainData.getChannelSix()
                + ",'channelSeven':" + brainData.getChannelSeven()
                + ",'channelEight':" + brainData.getChannelEight()
                + ",'channelNine':" + brainData.getChannelNine()
                + ",'channelTen':" + brainData.getChannelTen()
                + ",'channelEleven':" + brainData.getChannelEleven()
                + ",'channelTwelve':" + brainData.getChannelTwelve()
                + ",'channelThirteen':" + brainData.getChannelThirteen()
                + ",'channelFourteen':" + brainData.getChannelFourteen()
                + ",'channelFifteen':" + brainData.getChannelFifteen()
                + ",'channelSixteen':" + brainData.getChannelSixteen()
                +",'leftOrRightTrigger':" + brainData.getLeftOrRightTrigger() + ",'binaryClass':" + brainData.getBinaryClass()
                + ",'timeVaryingSignedDistance':" + brainData.getTimeVaryingSignedDistance() + ",'cybathlon':" + brainData.getCybathlon() + ",'sessionId':" + brainData.getSessionID() + "}";
                ws.Send(stringToSend);
                Thread.Sleep(4);
            }
        }

        public void OnApplicationQuit()
        {
            CloseServer();
        }

        public void CloseServer()
        {
            t.Abort();
            client.Close();
        }
    }
}
